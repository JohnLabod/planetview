Small program used to find the position of solar system bodies in the sky based on your location and the current time.
It uses the SPICE toolkit from NAIF and the python wrapper (spiceypy)  to get the location of the selected planet.

Add your location to the `lat`/`lon` variables, change `utc_local_time_difference` to your time zone. (Ex: If you use BRT then it will be -3 give or take DST) and specify a body by name or number.

Here is the summaries for the kernels that are in the kernel folder. Replace the `targ` variable with whatever body name you want. They are loaded in the kernel/Metak.txt file. I am currently using de435.bsp for planet positions, naif0011.tls for leap seconds, and pck00010.tpc for planetary constants.

More information

```
Summary for: de435.bsp
 
Bodies: MERCURY BARYCENTER (1) w.r.t. SOLAR SYSTEM BARYCENTER (0)
        VENUS BARYCENTER (2) w.r.t. SOLAR SYSTEM BARYCENTER (0)
        EARTH BARYCENTER (3) w.r.t. SOLAR SYSTEM BARYCENTER (0)
        MARS BARYCENTER (4) w.r.t. SOLAR SYSTEM BARYCENTER (0)
        JUPITER BARYCENTER (5) w.r.t. SOLAR SYSTEM BARYCENTER (0)
        SATURN BARYCENTER (6) w.r.t. SOLAR SYSTEM BARYCENTER (0)
        URANUS BARYCENTER (7) w.r.t. SOLAR SYSTEM BARYCENTER (0)
        NEPTUNE BARYCENTER (8) w.r.t. SOLAR SYSTEM BARYCENTER (0)
        PLUTO BARYCENTER (9) w.r.t. SOLAR SYSTEM BARYCENTER (0)
        SUN (10) w.r.t. SOLAR SYSTEM BARYCENTER (0)
        MERCURY (199) w.r.t. MERCURY BARYCENTER (1)
        VENUS (299) w.r.t. VENUS BARYCENTER (2)
        MOON (301) w.r.t. EARTH BARYCENTER (3)
        EARTH (399) w.r.t. EARTH BARYCENTER (3)
        Start of Interval (ET)              End of Interval (ET)
        -----------------------------       -----------------------------
        1549 DEC 31 00:00:00.000            2650 JAN 25 00:00:00.000


Summary for: de405.bsp
 
Bodies                                                 Start of Interval (ET)          End of Interval (ET)
-------                                                -----------------------------   -----------------------------
1 MERCURY BARYCENTER w.r.t. 0 SOLAR SYSTEM BARYCENTER  1950 JAN 01 00:00:41.183        2050 JAN 01 00:01:04.183
2 VENUS BARYCENTER w.r.t. 0 SOLAR SYSTEM BARYCENTER                Same coverage as previous object
3 EARTH BARYCENTER w.r.t. 0 SOLAR SYSTEM BARYCENTER                Same coverage as previous object
4 MARS BARYCENTER w.r.t. 0 SOLAR SYSTEM BARYCENTER                 Same coverage as previous object
5 JUPITER BARYCENTER w.r.t. 0 SOLAR SYSTEM BARYCENTER              Same coverage as previous object
6 SATURN BARYCENTER w.r.t. 0 SOLAR SYSTEM BARYCENTER               Same coverage as previous object
7 URANUS BARYCENTER w.r.t. 0 SOLAR SYSTEM BARYCENTER               Same coverage as previous object
8 NEPTUNE BARYCENTER w.r.t. 0 SOLAR SYSTEM BARYCENTER              Same coverage as previous object
9 PLUTO BARYCENTER w.r.t. 0 SOLAR SYSTEM BARYCENTER                Same coverage as previous object
10 SUN w.r.t. 0 SOLAR SYSTEM BARYCENTER                            Same coverage as previous object
199 MERCURY w.r.t. 1 MERCURY BARYCENTER                            Same coverage as previous object
299 VENUS w.r.t. 2 VENUS BARYCENTER                                Same coverage as previous object
301 MOON w.r.t. 3 EARTH BARYCENTER                                 Same coverage as previous object
399 EARTH w.r.t. 3 EARTH BARYCENTER                                Same coverage as previous object
499 MARS w.r.t. 4 MARS BARYCENTER  
```

The site I used for the formulas is here:
[How to compute planetary positions](http://www.stjarnhimlen.se/comp/ppcomp.html)
SPICE documentation:
[NAIF CSPICE Toolkit Hypertext Documentation](https://naif.jpl.nasa.gov/pub/naif/toolkit_docs/C/index.html)